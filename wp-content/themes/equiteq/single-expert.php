<?php

get_header();
$id = get_the_ID();
$expert = get_expert($id);
$title = strtoupper(get_field('title'));
$profile_image = get_field('profile_image');
$location_id = get_field('location');
$location = strtoupper(get_the_title($location_id));
$email = get_field('email');
$contact_no = get_field('contact_no');
$linkedin = get_field('linkedin');
// $industry_expertises = maybe_unserialize($expert->industry_expertise);
?>


<section>
    <div class="container no-pad-gutters">
        <div class="back mb-4 mb-md-5">
            <i class="fa fa-caret-left align-bottom" style="font-size: 22px;" aria-hidden="true"></i> <a href="#" onclick="history.back();" class="btn-outline-success text-uppercase px-0 ml-2">Back to team</a>
        </div>
        <!--May implement the expert's profile here -->
        <div class="row">
            <div class="col-md-4">
                <div class="team-bg-img">
                    <img src="<?php echo esc_url($profile_image); ?>" alt="<?php echo esc_attr($expert->post_title); ?>">

                </div>
            </div>
            <div class="col-md-8">
                <h1><?php echo strtoupper(esc_html($expert->post_title)); ?></h1>
                <h4><?php echo esc_html($title); ?></h4>
                <p class="expert-bdesc"><i class="fa fa-lg fa-map-marker mr-2 my-4" style="color: #afca0b;"></i><?php echo esc_html($location); ?></p>
                <div class="social-icons">
                    <ul class="experts-socials p-0 align-items-center">
                        <li><a href="mailto:<?php echo $email; ?>"><i class="fa fa-envelope" aria-hidden="true"></i></a></li>
                        <li><a href="tel:<?php echo $contact_no; ?>"><i class="fa fa-phone" aria-hidden="true"></i></a></li>
                        <li><a href="<?php echo $linkedin; ?>" target="_blank"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                    </ul>
                </div>
                <?php echo apply_filters('the_content', $expert->post_content); ?>
            </div>
        </div>

    </div>
</section>


<!--May implement the expert's industry expertise here -->

<?php
get_footer();
