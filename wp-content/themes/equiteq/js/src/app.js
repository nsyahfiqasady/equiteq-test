$(document).ready(function () {
    var selectedIndustry = '0';  
    var selectedLocation = '0';  

    $('.dropdown-menu a').click(function (event) {
        event.preventDefault();
        var selectedOption = $(this).text();
        $(this).closest('.dropdown').find('.dropdown-toggle').text(selectedOption);
    });

    $('.dropdown-menu[aria-labelledby="industrydd"] .dropdown-item').on('click', function () {
        selectedIndustry = $(this).data('industry').toString();  
        filterTeamBox();
    });

    $('.dropdown-menu[aria-labelledby="locdd"] .dropdown-item').on('click', function () {
        selectedLocation = $(this).data('location').toString();  
        filterTeamBox();
    });

    $('#quicksearch').on('keyup', function () {
        filterTeamBox();
    });

    function filterTeamBox() {
        var searchTerm = $('#quicksearch').val().toLowerCase();

        // console.log('Filter criteria:', {selectedIndustry, selectedLocation, searchTerm});

        $('.expert-box').each(function () {
            var industries = $(this).data('industry').toString();
            var locations = $(this).data('location').toString();

            // console.log('Expert:', {industries, locations});

            var industryMatch = selectedIndustry === '0' || industries.includes(selectedIndustry);
            var locationMatch = selectedLocation === '0' || locations.includes(selectedLocation);
            var name = $(this).text().toLowerCase();
            var searchMatch = name.indexOf(searchTerm) !== -1;

            // console.log('Match results:', {industryMatch, locationMatch, searchMatch});

            if (industryMatch && locationMatch && searchMatch) {
                $(this).css({ 'padding': '0 15px', 'max-height': '1000px', 'max-width': '1000px', 'opacity': '1', 'margin-bottom': '', 'overflow': 'hidden' });
            } else {
                $(this).css({ 'padding': '0', 'max-height': '0', 'max-width': '0', 'opacity': '0', 'margin-bottom': '0', 'overflow': 'hidden' });
            }
        });
    }

    filterTeamBox(); 
});
