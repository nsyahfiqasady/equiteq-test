<!-- The WordPress Primary Menu -->
<?php
$menu_locations = array(
    'footer' => array('name' => 'Services', 'container_id' => 'footer'),
    'footer_2' => array('name' => 'Sectors', 'container_id' => 'footer2'),
    'footer_3' => array('name' => '', 'container_id' => 'footer3')
);

foreach ($menu_locations as $location => $data) {
    echo '<div class="col-sm-12 col-md-auto pr-3" id="' . esc_attr($data['container_id']) . '">';

    echo '<a class="head">' . esc_html($data['name']) . '</a>';

    wp_nav_menu(
        array(
            'theme_location' => $location,
            'menu_class'     => 'list-unstyled',
        )
    );

    echo '</div>'; 
}
?>
