<?php
/* Template Name: Expert Page */
get_header();
$id = '363';
$page = get_post($id);

?>


<?php

/**Hero */
hm_get_template_part('template-parts/hero', ['page' => $page]);
?>

<section class="bg-dark-blue">
    <div class="container text-white no-pad-gutters">
        <h3 class="text-uppercase mb-4"><?php echo $page->intro_title ?></h3>
        <div class="row">
            <div class="col-md-8 mb-4">

                <?php echo $page->post_content ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-8">
                <h3>
                    FILTERS
                </h3>
                <div class="row">
                    <div class="industry-dd d-inline-block">
                        <div class="dropdown">
                            <button class="industry-btn dropdown-toggle" type="button" id="industrydd" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Sector
                            </button>
                            <ul class="dropdown-menu" aria-labelledby="industrydd">
                                <li><a class="dropdown-item" href="#" data-industry="0">All</a></li>
                                <?php
                                $industries = get_industries();
                                if (!empty($industries)) :
                                    foreach ($industries as $industry) :
                                        $industry_id = $industry->ID; 
                                        $industry_icon = get_field('industry_icon', $industry);
                                ?>
                                        <li>
                                            <a class="dropdown-item" href="#" data-industry="<?php echo esc_attr($industry_id); ?>">
                                                <?php if ($industry_icon) : ?>
                                                    <img class="mr-4" src="<?php echo esc_url($industry_icon['url']); ?>" alt="<?php echo esc_attr($industry_icon['alt']); ?>" width="20px">
                                                <?php endif; ?>
                                                <?php echo esc_html(get_the_title($industry)); ?>
                                            </a>
                                        </li>
                                <?php
                                    endforeach;
                                endif;
                                ?>
                            </ul>
                        </div>
                    </div>

                    <div class="location-dd d-inline-block">
                        <div class="dropdown">
                            <button class="location-btn dropdown-toggle" type="button" id="locdd" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Location
                            </button>
                            <ul class="dropdown-menu" aria-labelledby="locdd">
                            <li><a class="dropdown-item" href="#" data-location="0">All</a></li>

                                <?php
                                $locations = get_locations();
                                if (!empty($locations)) :
                                    foreach ($locations as $location) :
                                        $location_id = $location->ID; 
                                        echo '<li><a class="dropdown-item" href="#" data-location="' . esc_attr($location_id) . '">' . esc_html(get_the_title($location)) . '</a></li>';
                                    endforeach;
                                endif;
                                ?>
                            </ul>

                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-4">
                <h3>
                    SEARCH
                </h3>
                <div class="team-search">

                    <div class="input-group alt">
                        <input type="text" class="search-box" id="quicksearch">
                        <span class="input-group-append">
                            <i class="fa fa-search"></i>
                        </span>
                    </div>
                </div>
            </div>
        </div>

    </div>
</section>
<section class="bg-white">
    <div class="container no-pad-gutters">
        <div class="row">
            <?php
            $experts = get_experts();
            if ($experts) {
                foreach ($experts as $expert) {
                    $title = get_field('title', $expert->ID);
                    $profile_image = get_field('profile_image', $expert->ID);
                    $location_id = get_field('location', $expert->ID);
                    $location = get_the_title($location_id);
                    $email = get_field('email', $expert->ID);
                    $contact_no = get_field('contact_no', $expert->ID);
                    $linkedin = get_field('linkedin', $expert->ID);

                    $industries = get_field('industry_expertise', $expert->ID);
                    $industry_id = $industries->ID;
                    $location_id = $location_id->ID;

            ?>
                    <div class="col-md-3 col-xs-6 expert-box mt-3" data-industry="<?php echo esc_attr($industry_id); ?>" data-location="<?php echo esc_attr($location_id); ?>">
                        <a href="<?php echo get_permalink($expert->ID); ?>">
                            <img class="expert-img" src="<?php echo esc_url($profile_image); ?>" alt="<?php echo esc_attr($expert->post_title); ?>">
                        </a>
                        <h2 class="expert-title"><?php echo esc_html($expert->post_title); ?></h2>
                        <p class="expert-bdesc"><?php echo esc_html($title); ?></p>
                        <p class="expert-bdesc loc"><?php echo esc_html($location); ?></p>
                        <div class="social-icons">
                            <ul class="experts-socials p-0 align-items-center">
                                <li><a href="mailto:<?php echo $email; ?>"><i class="fa fa-envelope" aria-hidden="true"></i></a></li>
                                <li><a href="tel:<?php echo $contact_no; ?>"><i class="fa fa-phone" aria-hidden="true"></i></a></li>
                                <li><a href="<?php echo $linkedin; ?>" target="_blank"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                            </ul>
                        </div>
                    </div>
            <?php
                }
            } else {
                echo 'No experts found.';
            }
            ?>
        </div>
    </div>


<?php
get_footer();
?>